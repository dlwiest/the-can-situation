import React, { Component } from 'react';
import { Root } from 'native-base';
import { StyleProvider } from 'native-base';
import { Provider } from 'mobx-react';
import configureStores from './src/startup/configureStores';
import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/platform";
import AppNavigator from './src/navigation/AppNavigator';

export default class App extends Component {
  render() {
    const stores = configureStores();
    return (
      <Root>
        <StyleProvider style={getTheme(variables)}>
          <Provider {...stores}>
            <AppNavigator />
          </Provider>
        </StyleProvider>
      </Root>
    );
  }
}
