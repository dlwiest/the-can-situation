import * as Font from 'expo-font';
import { Asset } from 'expo-asset';
import React, { useState, useEffect } from 'react';
import { AsyncStorage, Text, Image } from 'react-native';
import { Provider } from 'mobx-react';
import { StyleProvider } from 'native-base';
import { FontAwesome } from '@expo/vector-icons';
import getTheme from "../../native-base-theme/components";
import variables from "../../native-base-theme/variables/platform";
import app from '../App';

export default stores => {
	return () => {
		const [didCacheFonts, setDidCacheFonts] = useState(false);
		const [didCacheImages, setDidCacheImages] = useState(false);
		const [didCheckLogin, setDidCheckLogin] = useState(false);
		const [isLoggedIn, setIsLoggedIn] = useState(false);

		useEffect(() => {
			checkLogin();
			cacheFonts();
			cacheImages();
		}, []);

		async function checkLogin() {
			const storedId = await AsyncStorage.getItem('facebookId');

			setIsLoggedIn(!!storedId);
			setDidCheckLogin(true);
		}

		async function cacheFonts() {
			const fonts = [ FontAwesome.font ];

			fonts.map(font => Font.loadAsync(font));

			setDidCacheFonts(true);
		}

		async function cacheImages() {
			const images = [
				require('../../assets/icon.png'),
				require('../../assets/splash.png'),
				'https://www.yellow5.com/pokey/archive/pokey1_1.gif',
			];

			images.map(image => {
				if (typeof image === 'string') return Image.prefetch(image);
				else return Asset.fromModule(image).downloadAsync();
			});

			setDidCacheImages(true);
		}

		if (!didCacheFonts || !didCacheImages || !didCheckLogin) return <Text>Loading</Text>

		return (
			<StyleProvider style={getTheme(variables)}>
				<Provider {...stores}>
					{app(isLoggedIn)}
				</Provider>
			</StyleProvider>
		);
	}
}
