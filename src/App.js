import React from 'react';
import { createStackNavigator, createSwitchNavigator, createAppContainer } from 'react-navigation';
import { Root } from 'native-base';
import LoginScreen from './screens/LoginScreen';
import ScannerScreen from './screens/ScannerScreen';
import InfoScreen from './screens/InfoScreen';

export default isLoggedIn => {
	const LoginStack = createStackNavigator({
		Login: { screen: LoginScreen },
	});

	const MainStack = createStackNavigator({
		Scanner: { screen: ScannerScreen },
		Info: { screen: InfoScreen },
	});

	const AppNavigator = createSwitchNavigator({
		Login: LoginStack,
		Main: MainStack,
	}, {
		initialRouteName: isLoggedIn ? "Main" : "Login",
	});

	const AppContainer = createAppContainer(AppNavigator);

	return (
		<Root>
			<AppContainer />
		</Root>
	);
}