import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import React, { useState, useEffect } from 'react';
import { Alert, AsyncStorage, Text, Button } from 'react-native';
import canSituationApi from '../../api/canSituation';

const ScannerScreen = () => {
	const [hasCameraPermission, setHasCameraPermission] = useState(false);
	const [isProcessingScan, setIsProcessingScan] = useState(false);

	useEffect(() => {
		requestPermissions();
	}, []);

	async function requestPermissions() {
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		setHasCameraPermission(status === 'granted');
	}

	async function handleBarCodeScanned(scan) {
		if (isProcessingScan) return;

		setIsProcessingScan(true);
		const result = await canSituationApi.getCan({ id: scan.data });
		
		if (result.status === 404) {
			// Register the can to the user
			const userId = await AsyncStorage.getItem('facebookId');
			console.log(canSituationApi.claimCan({ canId: scan.data, userId }));
			Alert.alert('Claimed the Can', 'You have claimed this can');
		} else {
			const body = await result.json();
			Alert.alert(`That can is registered to ${body.user.name}`);
		}

		setIsProcessingScan(false);
	}

	return (
		<React.Fragment>
			<Text>Scanner Screen</Text>
			{!hasCameraPermission && <Button title="Grant Permissions" onPress={requestPermissions} />}
			{hasCameraPermission &&
				<BarCodeScanner
					onBarCodeScanned={handleBarCodeScanned}
					style={{ height: 200, width: 200 }}
				/>
			}
		</React.Fragment>

	);
}

export default ScannerScreen;
