import React from 'react';
import { AsyncStorage, Alert, Button } from 'react-native';
import * as Facebook from 'expo-facebook';
import canSituationApi from '../../api/canSituation';

const LoginScreen = ({ navigation }) => {
	async function requestLogin() {
		try {
			const {
				type,
				token,
				expires,
				permissions,
				declinedPermissions,
			} = await Facebook.logInWithReadPermissionsAsync('2382731101765582', {
				permissions: ['public_profile'],
			});

			if (type === 'success') {
				const fbResponse = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
				const { id, name } = await fbResponse.json();
				const apiResponse = await canSituationApi.upsertUser({ id, name });

				await AsyncStorage.setItem('facebookId', id);
				navigation.navigate("MainStack");
			} else {
				Alert.alert('Permissions Required', 'Facebook login required for Can Situation');
			}
		} catch (error) {
			alert(error.message);
		}
	}

	return (
		<React.Fragment>
			<Button title="Login with Facebook" onPress={requestLogin} />
		</React.Fragment>
	);
}

export default LoginScreen;