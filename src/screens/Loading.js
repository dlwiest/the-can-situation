import React, { Component } from 'react';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import { AsyncStorage } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

export default class Loading extends Component {
  loadResources = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('../../assets/icon.png'),
				require('../../assets/splash.png'),
      ]),
      Font.loadAsync(FontAwesome.font),
    ]);
  }

  handleLoadingError = error => {
    console.warn(error);
  }

  handleLoadingFinish = async () => {
    const storedId = await AsyncStorage.getItem('facebookId');
    const { navigation } = this.props;
    if (storedId) navigation.navigate('MainStack');
    else navigation.navigate('LoginStack');
  }

  render() {
    return (
      <AppLoading
        startAsync={this.loadResources}
        onError={this.handleLoadingError}
        onFinish={this.handleLoadingFinish}
      />
    );
  }
}