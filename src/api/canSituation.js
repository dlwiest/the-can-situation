export default {
	upsertUser: ({ id, name }) => {
		const options = {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				userId: id,
				name,
			}),
		};

		return fetch('https://q84m2th6jc.execute-api.us-east-1.amazonaws.com/dev/users', options);
	},

	getCan: ({ id }) => {
		return fetch(`https://q84m2th6jc.execute-api.us-east-1.amazonaws.com/dev/cans/${id}`);
	},

	claimCan: ({ canId, userId }) => {
		const options = {
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				canId,
				userId,
			}),
		};

		return fetch('https://q84m2th6jc.execute-api.us-east-1.amazonaws.com/dev/cans', options);
	}
};
