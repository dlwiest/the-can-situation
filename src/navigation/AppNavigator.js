import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoginStack from './LoginStack';
import MainStack from './MainStack';
import Loading from '../screens/Loading';

export default createAppContainer(createSwitchNavigator({
  Loading,
  LoginStack,
  MainStack,
}));