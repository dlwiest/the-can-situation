import { createStackNavigator } from 'react-navigation';
import ScannerScreen from '../screens/ScannerScreen';
import InfoScreen from '../screens/InfoScreen';

export default createStackNavigator({
  Scanner: { screen: ScannerScreen },
  Info: { screen: InfoScreen },
});
