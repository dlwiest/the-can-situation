import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

export default class ProfileImage extends PureComponent {
  state = {
    loading: true,
    uri: null,
  };

  async componentWillMount() {
    const { userId, size } = this.props;
    console.log('making call');
    const { url } = await fetch(`https://graph.facebook.com/v3.3/${userId}/picture?height=${size}&width=${size}`);
    this.setState({ uri: url, loading: false });
  }
  render() {
    const { loading, uri } = this.state;
    const { size } = this.props;

    if (loading) return null;

    return <Image source={{ uri, cache: 'force-cache' }} style={{ height: size, width: size, borderRadius: Math.floor(size / 2) }} />;
  }
}

ProfileImage.propTypes = {
  userId: PropTypes.string.isRequired,
  size: PropTypes.number,
};

ProfileImage.defaultProps = {
  size: 200,
};
